## ML with Python Libraries

This is a set of examples which helped me get started with ML in Python using TensorFlow, NumPy, Pandas, Scikit-Learn and SciPy.

## Requirements to deploy

###### Predicting IMDB movie review as positive or negative
* Dependencies: tensorflow==2.2.0.

## Deployment

1. Clone project.
2. Create and activate a virtual environment.
2. Install the dependencies.
3. Run the corresponding python project.
