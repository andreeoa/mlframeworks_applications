# Goal: Text classification to identify good (1) and bad (0) movie reviews. Part 1-4 of video tutorial
# https://www.youtube.com/watch?v=6g4O5UOH304&t=1639s: Creating, training, testing, saving and loading model for
# prediction

import tensorflow as tf
from tensorflow import keras
import numpy as np

# Movie review sentiment classification dataset
data = keras.datasets.imdb

# Filtering the 10k most frequent words from the imdb dataset of 25,000 movies reviews
(train_data, train_labels), (test_data, test_labels) = data.load_data(num_words=10000)

# At this step, data processing could be required but the Keras dataset is already preprocessed and get_word_index
# returns a dictionary
word_index = data.get_word_index()  # Keys are words, values are their index: key= word (string) - value=
# index (integer)

# Iterating through the dictionary to move every index +3 to add three special characters
word_index = {k: (v + 3) for k, v in word_index.items()}
word_index["<PAD>"] = 0
word_index["<START>"] = 1
word_index["<UNK>"] = 2
word_index["<UNUSED>"] = 3

# Exchanging position between key and value, key = index (integer) - value = word (string)
reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

# Decoding data en readable words: def decode_review(text): return " ".join([reverse_word_index.get(i, "?") for i in
# text])  # replace missing word with question mark
# print(decode_review(test_data[0]))

# Uncomment the following lines to see that the dataset requires an additional processing step. An equal size for all
# the data samples is required to create the neural network. Therefore, the next preprocessing lines unify these
# lengths. We need to know how many input neurons are needed.
# lent1 = len(test_data[0])
# lent2 = len(test_data[1])

# With Keras is pretty simple:
train_data = keras.preprocessing.sequence.pad_sequences(train_data, value=word_index["<PAD>"], padding="post",
                                                        maxlen=250)
test_data = keras.preprocessing.sequence.pad_sequences(test_data, value=word_index["<PAD>"], padding="post", maxlen=250)

# Now every sample of the dataset is of length 250, to check it uncomment the following:
# lent1 = len(test_data[0])
# lent2 = len(test_data[1])

# Defining the architecture of the model:
model = keras.Sequential()  # The layers are added sequentially to build the text classifier
model.add(keras.layers.Embedding(10000, 16))  # This is a word embedding layer. The function of this layer is to group
# the words that have a similar meaning. The more training, the better groups. We use a 16-dimensional vector space for
# dense vector representation
model.add(keras.layers.GlobalAveragePooling1D())  # This layer summarises the average presence of a word (compression)
model.add(keras.layers.Dense(16, activation="relu"))  # Activation layer - 16 neurons with a rectified linear
# activation function to find patterns between distinct words
model.add(keras.layers.Dense(1, activation="sigmoid"))  # Defining one output tensor to be 0 or 1, mapping a review
# as positive or negative

# See the model summary by uncommenting the following:
# model.summary()

# Training the model
model.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])  # Using a stochastic gradient
# descent optimiser and binary cross entropy as loss function

# Splitting data in 2 sets: validation and training data
x_val = train_data[:10000]
x_train = train_data[10000:]

y_val = train_labels[:10000]
y_train = train_labels[10000:]

# Training the model:
history = model.fit(x_train, y_train, epochs=40, batch_size=512, validation_data=(x_val, y_val), verbose=1)
# The history is very useful if you want to visualize your training progress

# Evaluating the trained model on test data (not used for training). Its output is accuracy and loss,
# not prediction yet to the input data.
results = model.evaluate(test_data, test_labels)
print('Evaluation results for test loss, test acc:', results)


# Testing the model, generating predictions (probabilities - the output of the last layer) on new data:
# Converting testing data into readable words:
def decode_review(text):
    return " ".join([reverse_word_index.get(i, "?") for i in text])


test_review = test_data[0]  # first review of test_data
predict = model.predict([test_review])
print("Review: ")
print(decode_review(test_review))
print("Prediction: " + str(predict[0]))
print("Actual: " + str(test_labels[0]))
print(results)

# With the code above every time we want to make a prediction, a training of the model is required.
# Since there are models that can take even 1 year to train, a model should be saved after this one training and load
# when need it for prediction.

# Saving the model:
# model.save("model.h5") #h5 is the convention for a extension in Keras. The model is now saved in
# this directory as a binary file. Once the model is saved, every line above from line 50 to 84 is unnecessary. The
# following line would be required to load the model:
# model = keras.models.load_model("model.h5")
# You can train and save a model to tune its parameters (ex. number of neurons, models used)